# Deploy key

This module makes possible to generate private/public keys for any entity, for
example when you want to generate a key pair and display the public key
information per project node, or per user.

This is what powers the SSH keys part of Violinist.io.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/deply_key).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/deply_key).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

IInstall as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to /deploy-key/regenerate/{entity_type}/{id} to regenerate the key.


## Maintainers

- Eirik Morland - [eiriksm](https://www.drupal.org/u/eiriksm)
