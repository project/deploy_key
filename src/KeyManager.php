<?php

namespace Drupal\deploy_key;

use Codeaken\SshKey\SshKeyPair;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * KeyManager service used to manage keys.
 */
class KeyManager {

  /**
   * Cache tag so we can rely on that.
   */
  const CACHE_TAG_PREFIX = 'deploy_key';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * KeyManager constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, StateInterface $state, CacheTagsInvalidatorInterface $cacheTagsInvalidator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
  }

  /**
   * Get a key for an entity, and generate if we have to.
   *
   * Will also force regenerate if specified.
   */
  public function generateKeyForEntity(EntityInterface $entity, $force_regenerate = FALSE) {
    // First see if we have it.
    $key = $this->getKey($entity);
    if (NULL !== $key && !$force_regenerate) {
      return $key;
    }
    $key = $this->generateKey($entity);
    $this->state->set($this->getStorageKey($entity), $key);
    $this->cacheTagsInvalidator->invalidateTags([
      self::generateCacheKeyForEntity($entity),
    ]);
    return $key;
  }

  /**
   * Get cache key.
   */
  public static function generateCacheKeyForEntity(EntityInterface $entity) {
    return sprintf('%s_%s_%d', self::CACHE_TAG_PREFIX, $entity->getEntityTypeId(), $entity->id());
  }

  /**
   * Generates the key.
   */
  protected function generateKey(EntityInterface $entity) {
    // Go ahead and generate the key. We use the same recommendation as github
    // does, 4096 bit.
    // https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/
    return SshKeyPair::generate(4096);
  }

  /**
   * Get the actual key, if it exists.
   */
  public function getKey(EntityInterface $entity) {
    $store_key = $this->getStorageKey($entity);
    return $this->state->get($store_key);
  }

  /**
   * Get the key where we store it.
   */
  public function getStorageKey(EntityInterface $entity) {
    return sprintf('%s_%s', $entity->getEntityTypeId(), $entity->id());
  }

}
