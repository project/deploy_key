<?php

namespace Drupal\Tests\deploy_key\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the most basic functionality of the module.
 *
 * @group deploy_key
 */
class EnableTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'deploy_key',
  ];

  /**
   * Test enable.
   */
  public function testEnable() {
    self::assertTrue(TRUE);
  }

}
